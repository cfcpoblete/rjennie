# README

* Information

This microservice is for events/activity bookings

all connections to this webservice should focus on
 - online reservation system
 - booking software
 - any business which needs scheduling and managing their bookings.

* API Documentation

Staging: https://rjisoo.herokuapp.com/
Production:

View a specific Activity 
    GET    /activities/:activity_id/bookings

Create booking from an activity
    POST    /activities/:activity_id/bookings

Create booking from an activity
    POST    /activities/:activity_id/bookings
         params:
           {
             person_id: 3,
             time_in: '2018-11-15 18:06:23',
             time_out: '2018-11-15 18:07:23'
           }

Show client\'s blackpink booking
    GET    /activities/:activity_id/bookings/:id

Edit activity booking
    PATCH /activities/:activity_id/bookings/:id
          params:
            {
              person_id: 3
            }

Delete activity booking
    DELETE /activities/:activity_id/bookings/:id
          params:
            {
              person_id: 3
            }

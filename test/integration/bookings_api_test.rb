# frozen_string_literal: true

require 'test_helper'

class BookingsApiTest < ActionDispatch::IntegrationTest
  test 'view all bookings from an activity' do
    get activity_bookings_path(activities(:blackpink_concert))
    bookings = JSON.parse(response.body)
    assert_equal bookings.count, 2
  end

  test 'create booking from an activity' do
    post activity_bookings_path(activities(:blackpink_concert)),
         params:
           {
             person_id: 3,
             time_in: '2018-11-15 18:06:23',
             time_out: '2018-11-15 18:07:23'
           }
    assert_equal response.status, 201
    booking = JSON.parse(response.body)
    assert_equal booking['person_id'], 3
    assert_equal booking['time_in'], '2018-11-15T18:06:23.000Z'
    assert_equal booking['time_out'], '2018-11-15T18:07:23.000Z'
  end

  test 'show user\'s blackpink booking' do
    get activity_booking_url(activities(:blackpink_concert), bookings(:eric_blackpink))
    booking = JSON.parse(response.body)
    assert_equal response.status, 200
    assert_equal booking['person_id'], 1
    assert_equal booking['time_in'], '2018-11-15T18:06:23.000Z'
    assert_equal booking['time_out'], '2018-11-15T18:06:23.000Z'
  end

  test 'edit activity booking' do
    patch activity_booking_url(activities(:blackpink_concert), bookings(:eric_blackpink)),
          params:
            {
              person_id: 3
            }
    assert_not_nil Booking.find_by(person_id: 3)
  end

  test 'delete blackpink booking' do
    delete activity_booking_url(activities(:blackpink_concert), bookings(:eric_blackpink))
    assert_nil Booking.find_by(person_id: 1)
  end
end

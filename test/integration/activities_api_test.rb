# frozen_string_literal: true

require 'test_helper'

class ActivitiesApiTest < ActionDispatch::IntegrationTest
  test 'view all activities' do
    get '/activities'
    assert_equal response.status, 200
    assert response.body.include? 'Blackpink concert'
    assert response.body.include? 'Surprize Birthday Party'

    activities = JSON.parse(response.body)
    assert_equal activities.count, 2
  end

  test 'create activity is successfull if app_id is present' do
    post '/activities',
         params:
           {
             name: 'Removal Exams',
             app_id: 1,
             active: true,
             registration_open: Date.today,
             start: (Time.now + 1.day),
             end: (Time.now + 1.day + 1.hour)
           }

    assert_equal response.status, 201
    assert_equal Activity.last.name, 'Removal Exams'
  end

  test 'create activity fails if there is no app_id' do
    begin
      post '/activities',
           params:
             {
               name: 'Removal Exams',
               active: true,
               registration_open: Date.today,
               start: (Time.now + 1.day),
               end: (Time.now + 1.day + 1.hour)
             }
    rescue StandardError => error
      assert_equal error.to_s, "Validation failed: App can't be blank"
      assert_nil response
    end
  end

  test 'show blackpink activity' do
    get activities_url(activities(:blackpink_concert))
    activity = JSON.parse(response.body).first
    assert_equal response.status, 200
    assert_equal activity['name'], 'Blackpink concert'
    assert_equal activity['active'], true
    assert_equal activity['registration_open'], '2018-11-15T15:51:46.000Z'
    assert_equal activity['start'], '2019-11-15T11:51:46.000Z'
    assert_equal activity['end'], '2019-11-15T17:51:46.000Z'
  end

  test 'edit blackpink activity' do
    patch activity_url(activities(:blackpink_concert)),
          params:
            {
              name: 'Blackpink Fansclub'
            }

    assert_not_nil Activity.find_by(name: 'Blackpink Fansclub')
  end

  test 'delete blackpink activity' do
    delete activity_url(activities(:blackpink_concert))
    assert_nil Activity.find_by(name: 'Blackpink concert')
  end
end

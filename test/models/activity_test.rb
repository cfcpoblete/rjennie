# frozen_string_literal: true

require 'test_helper'

class ActivityTest < ActiveSupport::TestCase
  test 'if blackpink concert fixture exists' do
    blackpink = activities(:blackpink_concert)
    assert blackpink.active
    assert blackpink.name, 'Blackpink concert'
    assert_equal '2018-11-15 15:51', blackpink.registration_open.strftime('%Y-%m-%d %H:%M')
    assert_equal '2019-11-15 11:51', blackpink.start.strftime('%Y-%m-%d %H:%M')
    assert_equal '2019-11-15 17:51', blackpink.end.strftime('%Y-%m-%d %H:%M')
  end

  test 'test if blackpink fixture exists' do
    paolo = activities(:paolos_birthday)
    refute paolo.active
    assert paolo.name, 'Surprize Birthday Party'
    assert_equal '2019-01-15 06:00', paolo.registration_open.strftime('%Y-%m-%d %H:%M')
    assert_equal '2019-02-15 07:00', paolo.start.strftime('%Y-%m-%d %H:%M')
    assert_equal '2019-02-15 15:00', paolo.end.strftime('%Y-%m-%d %H:%M')
  end
end

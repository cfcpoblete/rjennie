# frozen_string_literal: true

require 'test_helper'

class BookingTest < ActiveSupport::TestCase
  test 'Blackpink concert has many bookings' do
    bookings = activities(:blackpink_concert).bookings
    assert_equal bookings.count, 2
  end

  test 'Paolos birthday has 0 bookings' do
    bookings = activities(:paolos_birthday).bookings
    assert_equal bookings.count, 0
  end
end

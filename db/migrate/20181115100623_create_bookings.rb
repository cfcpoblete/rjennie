class CreateBookings < ActiveRecord::Migration[5.2]
  def change
    create_table :bookings do |t|
      t.integer :person_id
      t.integer :activity_id
      t.datetime :time_in
      t.datetime :time_out

      t.timestamps
    end
  end
end

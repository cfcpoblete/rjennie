class AddAppIdToActivities < ActiveRecord::Migration[5.2]
  def change
    add_column :activities, :app_id, :integer
  end
end

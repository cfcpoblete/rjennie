Rails.application.routes.draw do
  resources :activities do
    resources :bookings
  end
  resources :syncs do
    collection do
      post 'activities' => 'syncs#activities'
      post 'bookings' => 'syncs#bookings'
    end
  end
end

class SyncsController < ApplicationController
  def activities
    Activity.destroy_all
    params["_json"].each do |activity|
      @activity = Activity.create!(
                    {
                      name: activity[:name],
                      registration_open: activity[:registration_open],
                      active: activity[:active],
                      start: activity[:start],
                      end: activity[:end],
                      app_id: 1
                    }
                  )
      @activity.id = activity[:id]
      @activity.save
    end
  end

  def bookings
    Booking.destroy_all
    params["_json"].each do |booking|
      @booking = Booking.create!(
                    {
                      person_id: booking[:person_id],
                      activity_id: booking[:activity_id],
                      time_in: booking[:time_in],
                      time_out: booking[:time_out]
                    }
                  )
      @booking.id = booking[:id]
      @booking.save
    end
  end
end

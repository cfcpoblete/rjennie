# frozen_string_literal: true

class BookingsController < ApplicationController
  before_action :set_booking, only: %i[show update destroy]

  # GET /bookings
  def index
    @activities = Booking.all
    json_response(@activities)
  end

  # POST /bookings
  def create
    @activity = Activity.find(params[:activity_id])
    @booking = @activity.bookings.create(booking_params)
    json_response(@booking, :created)
  end

  # GET /bookings/:id
  def show
    json_response(@booking)
  end

  # PUT /bookings/:id
  def update
    @booking.update(booking_params)
    head :no_content
  end

  # DELETE /bookings/:id
  def destroy
    @booking.destroy
    head :no_content
  end

  private

  def booking_params
    params.permit(:person_id, :time_in, :time_out)
  end

  def set_booking
    @booking = Booking.find(params[:id])
  end
end

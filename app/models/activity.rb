# frozen_string_literal: true

class Activity < ApplicationRecord
  has_many :bookings
  validates_presence_of :app_id
end
